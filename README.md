# Curso GIT DEVMEDIA

[Pasta Compartilhada](https://drive.google.com/drive/folders/1HxeFkT0GoKLog9xuRZuAViJZubyBc_bx?usp=sharing)

## Introdução
---

#### 1. Curso de Git: Introdução 

* Esta é a aula introdutória do curso de controle de versões distribuído com Git. Nela, faço uma breve apresentação dos tópicos que serão discutidos no decorrer do curso e aproveito para fazer uma breve explanação do que vem a ser o Git e quais as possiveis classificações usadas para este software. 

- 15:03 min 

#### 2. Ferramentas de Controle de Versão

* Nesta vídeo aula, discuto a evolução de forma resumida dos sistemas de controle de versão ao longo do tempo, desde o CVS até o Git, passando pelo SVN e BitKeeper. Aproveito para discutir sobre o público alvo para utilização do Git e inicio uma discussão do que vem a ser controle de versão distribuído.

- 15:00 min 

#### 3. Instalação do GIT

* Nesta vídeo aula, mostro como fazer o download e instalação do Git para Windows. Após correta instalação, exibo como fazer as configurações iniciais indicando o nome, email e editor padrão em nível de usuário do sistema. 

- 15:47 min

#### 4. Primeiro Commit

* Nesta vídeo aula, mostro como iniciar um repositório Git para um novo projeto. Após iniciado o repositório, exibo o workflow básico necessário para se colocar um arquivo sob controle do Git através dos comandos git add e git commit. Por fim discuto sobre como acessar o log de commits. 

- 17:38 min

#### 5. Processo de Commit

* Agora que já aprendemos como efetuar nosso primeiro commit, nesta vídeo aula, mostro como o git trabalha com o processo de commit conhecido como "3-trees". Aproveito para ilustrar os conceitos de SHA-1 e HEAD.

- 16:02 min


#### 6. Edição de Arquivos

* Nesta vídeo aula, mostro como fazer a edição de um arquivo existente no repositório e as consequências oriundas desta alteração no que tange o sistema de controle de versão Git. 

- 14:54 min

#### 7. Diferenças entre versões de arquivos

* Nesta gravação mostro como visualizar as alterações feitas entre duas versões de um determinado arquivo. Para isso, usarei o famoso comando "diff", muito conhecido por todos aqueles com experiência na platafor UNIX.

- 15:07 min

#### 8. Remoção de arquivos 

* Nesta gravação mostro como trabalhar com o Git no momento que precisamos remover arquivos de nosso projeto. Apresento duas soluções possíveis. 

- 15:07 min

## Manipulando arquivos
---

#### 9. Curso de Git: Renomeação e remoção de arquivos

* Nesta gravação mostro como trabalhar com o Git no momento que precisamos remover arquivos de nosso projeto. Apresento duas soluções possíveis. 

- 14:58 min

#### 10. Desfazendo alterações

* Nem sempre estamos satisfeitos com nossas alterações feitas em arquivos. Nesse contexto, nesta aula mostro duas formas de desfazermos alterações indevidas: alterações no diretório de trabalho e alterações presentes na stage area. 

- 14:56 min

#### 11. Desfazendo alterações comitadas

* Nesta aula mostro como desfazer, não apenas a mensagem, mas também o conteúdo do último commit feito no repositório.

- 14:57 min

#### 12. Comando git-clean

* Nesta aula mostro como remover arquivos ainda não presentes no repositório com o comando git-clean e também mostro como recuperar versões antigas do repositório. 

- 16:43 min

#### 13. Comando git-reset

* Nesta gravação mostro como desfazer commits com git reset. Existem três modos de operação deste comando. Nesta aula mostro os modos "soft" e "mixed".

- 17:16 min

#### 14. Desfazendo comits

* Nesta aula mostramos como desfazer commits com git reset. Existem 3 modos de operação deste comando. Nesta aula mostro o modo Hard e as implicações de seu uso. 

- 17:16 min 

## Branch
---
 
#### 15. Curso de Git: Branch

* Nesta aula é apresentado o importante conceito de branch, indicando seus usos e principais comandos git para auxiliar o desenvolvedor no dia-a dia. Em particular, é destacado como criar e alternar entre branches bem como a influência disso no ponteiro HEAD.

- 15:12 min

#### 16. Alternando entre Branches

* Nesta aula são apresentadas as preocupações necessárias que se deve tomar ao se alternar entre branches. Em especial é simulada a alternância entre branches com o diretório de trabalho com alterações não comitadas.

- 17:01 min 

#### 17. Comparando Branches

* Nesta aula será demonstrado como se pode comparar o conteúdo entre branches além de exibir uma maneira para renomeá-los.

- 15:40 min 

#### 18. Removendo Branches

* Nesta aula será apresentado o comando necessário para a remoção de branches e aproveito para fazer um resumo dos tópicos sobre branches nas últimas aulas. 

- 14:01 min 

#### 19. Uso do Branch

* Nesta aula são apresentadas duas formas do comando merge usadas pelo git quando não há conflito entre os conteúdos de branches distintas. 

- 15:00 min

#### 20. Merge com conflito

* Nesta aula é apresentado como se pode lidar com conflitos no momento de se fazer o merge entre branches. 

- 15:26 min

#### 21. Repositórios Remotos

* Nesta aula é iniciada a discussão sobre repositórios remotos. Para isso, é feita uma introdução ao assunto e é apresentado o GitHub, mostrando como criar um repositório remoto e como configurar o acesso a este repositório a partir de uma máquina cliente. 

- 15:00 min 

#### 22. Git Push e Git Clone

* Nesta aula é apresentado como utilizar os comandos git push e git clone. O primeiro permite enviar change sets para um repositório remoto enquanto que o segundo permite obter uma versão do projeto a partir de um respositório remoto. 
 
- 15:00

#### 23. Rastreamento de branches remotas

* Nesta videoaula é apresentado como manter o sincronismo entre branches remotas e locais além de discutir o comando push. 

- 15:55 min 

#### 24. Comando Fetch

* Nesta videoaula é apresentado como usar o comando fetch para buscar atualizações em branches remotas. 

- 15:03 min 

#### 25. Encerramento do curso

* Nesta última videoaula do nosso curso é apresentado como remover branches remotas e direcionar os estudos para aqueles que pretendem entender com mais profundidade os conceitos associados ao Git.

- 15:03 min

### Comandos
---

1. git-help - Exibe informações de ajuda sobre o Git
 
2. git init - Cria um repositório do Git a partir do diretório que está
 
3. git-clean - Remove arquivos não rastreados da árvore de trabalho
 
4. git-log - Mostra logs de confirmação
 
5. git-status - Mostra o status da árvore de trabalho
 
6. git ls - Mostra informações sobre arquivos no índice e na árvore de trabalho
